const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const activityPeriodSchema = new Schema({
    start_time: { type: Date, required: true},
    end_time: { type: Date, required: true}
});

const activitySchema = new Schema({
    activity_periods: { activityPeriodSchema },
}, {
    timestamps: true
});

const Activity = mongoose.awsMongo.model('activities', activitySchema);

module.exports = Activity;
