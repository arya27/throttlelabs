const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const userSchema = new Schema({
    real_name: { type: String, required: true, unique: true, trim: true, minlength: 3 },
    tz: { type: String, required: true, unique: true, trim: true, minlength: 7 }
}, {
    timestamps: true
});

const User = mongoose.awsMongo.model('users', userSchema);

module.exports = User;
