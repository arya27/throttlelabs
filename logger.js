let demoLogger = (req, res, next) => {
    let method = req.method;
    let url = req.url;
    let status = res.statusCode;
    let log = `Received ${method}:${url} ${status}`;
    console.log(log);
    next();
  };
  
  module.exports = demoLogger;
