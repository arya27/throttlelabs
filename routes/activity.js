const router = require('express').Router();
const Activity = require('../models/activity-model');
const demoLogger = require('../logger');

router.route('/:id').get(demoLogger, (req, res) => {
    Activity.findById(req.params.id)
    .then((activities) => {
        res.json(activities);
    })
    .catch((err) => {
        res.status(400).json(`Error: ${err}`);
    })
});

module.exports = router;
