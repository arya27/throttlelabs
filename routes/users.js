const router = require('express').Router();
const User = require('../models/user-model');
const demoLogger = require('../logger');

router.route('/').get(demoLogger, (req, res) => {
    User.find()
    .then((users) => {
        res.json(users);
    })
    .catch((err) => {
        res.status(400).json(`Error: ${err}`);
    })
});

router.route('/:id').get(demoLogger, (req, res) => {
    User.findById(req.params.id)
    .then((user) => {
        res.json(user);
    })
    .catch((err) => {
        res.status(400).json(`Error: ${err}`);
    })
});

module.exports = router;
