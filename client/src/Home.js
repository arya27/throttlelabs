import React, { Component } from 'react';
import Axios from 'axios';
import ActivityList from './ActivityPeriod';

const Members = props => (
    <div>
        <h3>Member Details</h3>
        {props.getMemberList()}
    </div>
)

class Home extends Component {

    constructor(props) {
        super(props)

        this.state = {
            showMembers: true,
            members: [],
            memberId: ''
        }
    }

    componentDidMount() {
        Axios.get("/users")
            .then((res) => {
                console.log(res.data)
                if (res.data.length > 0) {
                    this.setState({
                        members: res.data
                    })
                }
            });
    }

    handleMemberClick = (id) => {
        this.setState({
            showMembers: false,
            memberId: id
        });
    }

    getMemberList = () => {
        const memberList = this.state.members.map(member => {
            return (
                <button className="waves-effect waves-light btn-large truncate col s4" style={{marginRight: '10px'}}
                    key={member._id} onClick={() => { this.handleMemberClick(member._id); }}>{member.real_name}</button>
            );
        });

        return (
            <div className="row">
                {memberList}
            </div>
        )
    }

    showMembers = () => {
        this.setState({
            showMembers: true
        });
    }

    render() {
        const homeContent = this.state.showMembers ? (
            this.state.members.length === 0 ? (
                <div className="progress">
                    <div className="indeterminate"></div>
                </div>
            ) : (
                    <Members getMemberList={this.getMemberList} />
                )
        ) : (
                <div>
                    <ActivityList id={this.state.memberId} showMembers={this.showMembers} />
                </div>
            );
        return (
            <div>
                {homeContent}
            </div>
        );
    }
}

export default Home;
