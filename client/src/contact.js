import React from 'react';

const Contact = () => {
    return (
        <div>
            <br></br>
            <div >
                <h4 className="center-align">This project is developed by Anmol Arya</h4>
            </div>
            <br></br>
            <br></br>
            <div >
                <h5 className="center-align">You can reach me out via below means:</h5>
            </div>
            <div >
                <h5 className="center-align">Contact: 8961960404</h5>
            </div>
            <div >
                <h5 className="center-align">Email-Id: aryanunnown07@gmail.com</h5>
            </div>
        </div>
    )
}

export default Contact;