import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import Navbar from './Navbar';
import Contact from './contact';
import Home from './Home';

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <Router>
                <div className="container">
                    <Navbar />
                    <Route path='/' exact component={Home} />
                    <Route path='/contact' component={Contact} />
                </div>
            </Router>
        )
    }
}


export default App;