import React, { Component } from 'react';
import axios from 'axios';
import DateTimePicker from 'react-datetime-picker';

const Activity = props => (
    <tr>
        <td>{props.startTime}</td>
        <td>{props.endTime}</td>
    </tr>
);

export default class ActivityList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            real_name: '',
            tz: '',
            activity_period: [],
            filtered_activity_period: [],
            startDate: new Date(),
            endDate: new Date()
        }
    }

    componentDidMount() {
        axios.get("/users/" + this.props.id)
            .then((res) => {
                console.log(res.data)
                this.setState({
                    real_name: res.data.real_name,
                    tz: res.data.tz
                })
                console.log(this.state);
            });

        axios.get('/activity/' + this.props.id)
            .then(response => {
                console.log(`activity response: ${JSON.stringify(response.data)}`);
                this.setState({
                    activity_period: response.data.activity_periods,
                    filtered_activity_period: response.data.activity_periods
                });
                console.log(this.state);
            })
            .catch(function (error) {
                console.log(error);
            })

    }

    getActivityList() {
        let count = 0;
        return this.state.filtered_activity_period.map(activity => {
            let startDate = new Date(activity.start_time);
            let endDate = new Date(activity.end_time);
            startDate = startDate.toLocaleString("en-US", { timeZone: this.state.tz });
            endDate = endDate.toLocaleString("en-US", { timeZone: this.state.tz });

            return <Activity startTime={startDate} endTime={endDate} key={count++} />;
        })
    }

    getFilteredActivityList() {
        let filteredActivityList = [];
        filteredActivityList = this.state.activity_period.filter(activity => {
            let startDate = new Date(activity.start_time);
            let endDate = new Date(activity.end_time);

            return ((startDate >= this.state.startDate && startDate <= this.state.endDate) || (endDate >= this.state.startDate && endDate <= this.state.endDate));
        });

        this.setState({
            filtered_activity_period: filteredActivityList
        });
    }

    handleFilterActivity = (e) => {
        e.preventDefault();

        console.log(`Start Date: ${this.state.startDate}, End Date: ${this.state.endDate}`);
        this.getFilteredActivityList();
    }

    onChangeStartDate = (date) => {
        this.setState({
            startDate: date
        });
    }

    onChangeEndDate = (date) => {
        this.setState({
            endDate: date
        });
    }

    createFormForFilterActivity = () => {
        return (
            <form onSubmit={this.handleFilterActivity}>
                <table className="table">
                    <tbody>
                        <tr>
                            <td>
                                <div className="form-group">
                                    <label>State Date:</label>
                                    <div className='aligh-left' key='1'>
                                        <DateTimePicker format='yyyy-MM-dd HH:mm:ss'
                                            value={this.state.startDate}
                                            onChange={this.onChangeStartDate} />
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div className="form-group">
                                    <label>End Date:</label>
                                    <div className='aligh-right'>
                                        <DateTimePicker format='yyyy-MM-dd HH:mm:ss'
                                            value={this.state.endDate}
                                            onChange={this.onChangeEndDate} />
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div className="form-group">
                                    <input type="submit" value="Filter Activity" className="btn btn-primary" />
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </form>
        );
    }

    render() {
        const activityList = this.state.real_name !== '' && this.state.activity_period !== [] ? (
            <div>
                {this.createFormForFilterActivity()}

                <h3>Activity Periods for {this.state.real_name}.</h3>
                <h6>TimeZone: {this.state.tz}</h6>
                <table className="table">
                    <thead className="thead-light">
                        <tr>
                            <th>Start Time</th>
                            <th>End Time</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.getActivityList()}
                    </tbody>
                </table>
                <br/>
                <button className="waves-effect waves-light btn" onClick={this.props.showMembers}><i className="material-icons left">chevron_left</i>Go Back</button>

            </div>
        ) : (
                <div className="progress">
                    <div className="indeterminate"></div>
                </div>
            );

        return (
            <div>
                {activityList}
            </div>
        )
    }
}
