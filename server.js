const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const morgan = require('morgan');

require('dotenv').config();

const app = express();
const PORT = process.env.PORT || 8080;

app.use(cors());

const MONGO_URI = process.env.MONGO_URI || 'mongodb+srv://dbanmol:Q5nJvIJ1zpW2Sk1X@anmol-mongo.sknqh.mongodb.net/fullThrottle?retryWrites=true&w=majority';

mongoose.awsMongo = mongoose.createConnection(MONGO_URI, {useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true});
mongoose.awsMongo.once('open', () => {
    console.log(`connection to mongoDB established successfully`);
});

const activityRoutes = require('./routes/activity');
const userRoutes = require('./routes/users');

app.use(morgan('tiny'));
app.use('/activity', activityRoutes);
app.use('/users', userRoutes);

if (process.env.NODE_ENV === 'production') {
    app.use(express.static('client/build'));
}

app.listen(PORT, () => {
    console.log(`https://localhost:${PORT} Server started successfully`);
});
