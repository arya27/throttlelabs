## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.
5. Do **npm install** to install dependencies related to back-end nodejs server.
6. Run **npm test** to start express server
7. Created user and activity model based on mongo collection
8. Added basic get route for user and activity
9. All client related code are in client folder.
10. App component has all the other components of the app gets rendered.. Navbar component is present in the hompage of the app and linked to the contact page component.
11. Contact component file consist of the contact details of the app developer.
12. Home component file consist of the member details which are fetched from the API call using 'Axios'. Member names gets populated on the web page, on click to the 
    member name page it displays the active time details of each members.
13. ActivityPeriod component file stores the activity times of all the members from API call using 'Axios' and displays accordingly.It has filter property 
    which filters the date wise data from the members.
14. Used heroku-cli to deploy this project.